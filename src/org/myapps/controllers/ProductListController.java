package org.myapps.controllers;

import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.myapps.services.ProductService;

import com.opensymphony.xwork2.ModelDriven;

public class ProductListController implements ModelDriven<Object> {

	private Object model;

	public Object getModel(){
		// This method is called through the AJAX
			return new ProductService().findAll();
	}

	public HttpHeaders create() {
		return new DefaultHttpHeaders("show");
	}


	public HttpHeaders show() {
		return new DefaultHttpHeaders("show");
	}

	public HttpHeaders destroy() {
		return new DefaultHttpHeaders("show");
	}

	public HttpHeaders index() {
		return new DefaultHttpHeaders("show");
	}
}

