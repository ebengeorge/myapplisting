package org.myapps.beans;
/*
 * This is the object that we will be using, in real life scenario this could be a pojo.
 */
public class Product {
	private int productId;
	private String productCd;
	private String productName;
	private String productDesc;
	
	
	
	public Product(int productId, String productCd, String productName, String productDesc) {
		super();
		this.productId = productId;
		this.productCd = productCd;
		this.productName = productName;
		this.productDesc = productDesc;
	}



	public int getProductId() {
		return productId;
	}



	public void setProductId(int productId) {
		this.productId = productId;
	}



	public String getProductCd() {
		return productCd;
	}



	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public String getProductDesc() {
		return productDesc;
	}



	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}



	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productCd=" + productCd + ", productName=" + productName
				+ ", productDesc=" + productDesc + "]";
	}
}
