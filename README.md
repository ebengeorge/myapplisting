# README #

This is a J2EE application that provides a REST service to display product listing

### How do I get set up? ###

This is a J2EE Struts2 application that runs on Apache Tomcat7, Written in Java7; all dependencies are built into the project.
Once set up on local environment hit localhost:8080/MyAppListing/jsp/listing.jsp on your browser.