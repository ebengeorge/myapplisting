<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>MyApp Listing</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/listing.js"></script>
<script type="text/javascript" src="../js/jquery.slimscroll.min.js"></script>

<style type="text/css">
.collapse {
	padding: 0.7em !important;
    background: rgba(11, 11, 11, 0.1) !important;
}
</style>
</head>
<body>
	<div class="container">
		<h2>MyApps Product Listing</h2>
		<ul id="prod-list" class="list-group">
		<!-- This is where the listing gets populated -->
		</ul>
	</div>
</body>
</html>