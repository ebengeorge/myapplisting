$(function(){
// fetching data from ajax so that it isn't accessible to a bot	 
	  $.ajax({
	        url: "http://localhost:8080/MyAppListing/api/intro/getModel.json"
	    }).then(function(data) {
	    	var jsonData = $.parseJSON(data);
	    	if(jsonData.length == 0){
	    		// if no element is found in response , notify user.
	    		$('#prod-list').append('<li class="list-group-item" >No data available</li>');
	    	} else{
	    		$(jsonData).each(function(id,obj){
	    			//If the size is 1 or greater, use the data to populate a collapsible product listing
	    			var html=  '<li class="list-group-item" data-toggle="collapse" data-target="#'+obj.productCd+'"><strong>'+obj.productName+'</strong></li>';
	    			html += '<div id="'+obj.productCd+'" class="collapse">'+obj.productDesc+'</div>';
	    			$('#prod-list').append(html);
	    		});
	    	}
	    	$('#prod-list').slimScroll({
	            height: '250px'
	        });
	    })
	    .error(function(){
	    	$('#prod-list').append('<li class="list-group-item" >No data available</li>');
	    });
});
